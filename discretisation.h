#ifndef DISCRETISATION_H
#define DISCRETISATION_H
#include "QVector"
#include "courbeparametrique.h"
#include "surfaceparametrique.h"
#include <functional>
#include <QDebug>

QVector<float>* discretize(Point &p, float r, float v, float b);
QVector<float>* discretize(CourbeParametrique &p, /*std::function<float(float)> lambda, */float step, float begin, float end);
QVector<float>* discretize(SurfaceParametrique &p, /*std::function<float(float)> lambda, */float stepU, float stepV, float begin, float end);

QVector<float>* discretizeQUADS(SurfaceParametrique &p, float step, float begin, float end);

template<typename T> bool helper(T val);
QVector<float>* findPoint(SurfaceParametrique &p, float u, float v);


/*class Discretisation
{
public:
    Discretisation();
};*/

#endif // DISCRETISATION_H
