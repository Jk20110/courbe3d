#ifndef BERNSTEIN_H
#define BERNSTEIN_H
#include <QtMath>
#include <QDebug>

float fac[13] = {1.0f, 1.0f, 2.0f, 6.0f, 24.0f, 120.0f, 720.0f,
                 5040.0f, 40320.0f, 362880.0f, 3628800.0f, 39916800.0f, 479001600.0f};

float f(int i){
    return fac[i];
}


float bernsteinCompute(int i, int n, float x){
    qDebug() << "i, n, t" << i << n << x;
    return (f(n)/(f(i)*f(n-i))) * powf(x,i) * powf(1.0f - x, n - i);
}



#endif // BERNSTEIN_H
