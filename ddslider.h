#ifndef DDSLIDER_H
#define DDSLIDER_H

#include <GL/glut.h>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QKeyEvent>
#include <QVector2D>
#include <QLabel>


class DDSlider : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:

    explicit DDSlider(QWidget *parent = 0);
    ~DDSlider() override;
    QVector2D * current = new QVector2D(0.0f, 0.0f);

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
     void mouseMoveEvent(QMouseEvent *ev)override;
private:
    bool state;
    bool mousePressed=false;

signals:
    void mouseClicked();
    void mouseReleased();
    void posChanged(float x, float y);
};

#endif // DDSLIDER_H
