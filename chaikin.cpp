#include "chaikin.h"
#include <qdebug.h>


Chaikin::Chaikin(QVector<Point> points)
{
    this->points = points;
}

Point Chaikin::getPoint(float x){
    qDebug() << x;
    QVector<Segment> segments = Segment::extractSegments(points);

    if(x <= 0){x=0;}
    if(x >= 1){x=1;}

    segments = iterations(segments, 3);

    float total_length=0;
    for(Segment s: segments){
        total_length += s.length();
    }
    float searched_length = total_length*x;
    for(Segment s: segments){
        if(searched_length<s.length()){
            return s.getPoint(searched_length/s.length());
        }
        searched_length -= s.length();
    }

    return Point();
}

QVector<Segment> Chaikin::iterations(QVector<Segment> s, int level){
    if(level<1 || s.size()<2){return s;}
    QVector<Segment> segments = QVector<Segment>();

    Point current = s[0].getPoint(2.f/3.f);
    segments.push_back(*Segment::createSegment(s[0].getPoint(1.f/3.f), current));

    Point next_begin, next_end;
    for(int i=0; i<s.size()-1;i++){
        next_begin = s[i+1].getPoint(1.f/3.f);
        next_end   = s[i+1].getPoint(2.f/3.f);

        segments.push_back(*Segment::createSegment(current, next_begin));
        segments.push_back(*Segment::createSegment(next_begin, next_end));

        current = next_end;
    }

    return iterations(segments, level-1);
}
