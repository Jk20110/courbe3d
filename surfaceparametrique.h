#ifndef SURFACEPARAMÉTRIQUE_H
#define SURFACEPARAMÉTRIQUE_H

#include "point.h"
#include <qvector.h>

class SurfaceParametrique
{
public:
    SurfaceParametrique(){}
    //~CourbeParametrique(){};
    virtual Point getPoint(float x, float y)=0;
};

#endif // SURFACEPARAMÉTRIQUE_H
