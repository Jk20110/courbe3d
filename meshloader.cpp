#include "meshloader.h"

MeshLoader::MeshLoader(QString filename) : filename(filename) {}

//Charge un maillage et liste tous ses points
//Un peu nul pour l'instant.
QVector<QVector3D> MeshLoader::parse_mesh_file(MyMesh _mesh)
{
    if (!OpenMesh::IO::read_mesh(_mesh, filename.toStdString()))
    {
      std::cerr << "read error\n";
      exit(1);
    }
    QVector<QVector3D> controlPoints;
    std::cerr << "filename :" << this->filename.toStdString();
    std::cout << "Mesh stats : ";
    for (MyMesh::VertexIter v_it=_mesh.vertices_begin(); v_it!= _mesh.vertices_end(); ++v_it) {
        VertexHandle vh = *v_it;
        MyMesh::Point p = _mesh.point(vh);
        std::cout << p[0] << " " << p[1] << " " << p[2] << "\n";
        controlPoints.push_back(QVector3D(p[0], p[1], p[2]));
    }
    return controlPoints;
}
