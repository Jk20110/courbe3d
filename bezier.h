#ifndef BEZIER_H
#define BEZIER_H
#include "courbeparametrique.h"
#include "segment.h"
#include <QVector>

class Bezier : public CourbeParametrique
{
public:
    Bezier(QVector<Point> points);
    Point getPoint(float x)override;
private:
    Point iterationCasteljaud(float x, QVector<Segment> segments, int level);
    QVector<Point> points;
};

#endif // BEZIER_H
